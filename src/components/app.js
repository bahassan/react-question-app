'use strict';

var React = require('react/addons');
var HomeView = require('./home-view');

// CSS
require('normalize.css');
require('../styles/main.scss');

var QuestionData = [
    {id: 1, isHidden: false, questionType: 'textarea', question: 'Question 1', answer: 'A sample answer'},
    {id: 2, isHidden: true, questionType: 'boolean', question: 'Question 2', answer: 'Yes'},
    {id: 3, isHidden: false, questionType: 'select', question: 'Question 3', answer: 'Answer 2', options: ['Answer 1', 'Answer 2']},
    {id: 4, isHidden: false, questionType: 'textarea', question: 'Question 4', answer: 'A sample answer'},
    {id: 5, isHidden: false, questionType: 'boolean', question: 'Question 5', answer: 'Yes'},
    {id: 6, isHidden: false, questionType: 'select', question: 'Question 6', answer: 'Answer 1', options: ['Answer 1', 'Answer 2']},
    {id: 7, isHidden: false, questionType: 'textinput', question: 'Question 7', answer: 'A sample answer'},
    {id: 8, isHidden: true, questionType: 'boolean', question: 'Question 8', answer: 'Yes'},
    {id: 9, isHidden: false, questionType: 'checkbox', question: 'Question 9', answer: 'Answer 1', options: ['Answer 1', 'Answer 2', 'Answer 3', 'Answer 4']},
    {id: 10, isHidden: false, questionType: 'select', question: 'Question 10', answer: 'Answer 2', options: ['Answer 1', 'Answer 2']}
];

var App = React.createClass({
    render: function() {
        return (
            <HomeView data={QuestionData} />
        );
    }
});

module.exports = App;
