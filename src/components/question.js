'use strict';

var React = require('react/addons');
var ValidationMixin = require('./mixins/ValidationMixin');
var Joi = require('joi');

var map = {
    1: 'one'
};

var Question = React.createClass({
    mixins: [ValidationMixin, React.addons.LinkedStateMixin],
    validatorTypes: {
        one: Joi.string().required()
    },
    getInitialState: function() {
        return {
          one: null
        };
      },
    handleChange: function(event) {
        var answer = event.target.value;
        this.props.onChange(this.props.id, answer);
    },
    render: function() {
        switch (this.props.data.questionType) {
            case 'boolean':
                return this.radioInlines(this.props.id, this.props.data.question, ['Yes', 'No'], {
                    defaultValue: this.props.data.answer
                });
            case 'select':
                return this.select(this.props.id, this.props.data.question, this.props.data.options, {
                    defaultValue: this.props.data.answer
                });
            case 'textarea':
                return this.textarea(this.props.id, this.props.data.question, {
                    defaultValue: this.props.data.answer
                });
            case 'textinput':
                return this.textInput(this.props.id, this.props.data.question, {
                    defaultValue: this.props.data.answer
                });
            case 'checkbox':
                return this.checkbox(this.props.id, this.props.data.question, this.props.data.options, {
                    defaultValue: this.props.data.answer
                });
        }
    },
    renderHelpText: function(message) {
        console.log(message);
        return (
            React.createElement('span', {className: 'help-block'}, message)
        );
    },
    textInput: function(id, label, kwargs) {
        var input = <input type='text' className='form-control' id={id} ref={id} valueLink={this.linkState(map[id])} onBlur={this.handleValidation(map[id])} defaultValue={kwargs.defaultValue}/>;
        return this.formField(id, label, input, kwargs);
    },
    textarea: function(id, label, kwargs) {
        var textarea = <textarea className="form-control" id={id} ref={id} valueLink={this.linkState(map[id])} onBlur={this.handleValidation(map[id])} defaultValue={kwargs.defaultValue}/>;
        return this.formField(id, label, textarea);
    },
    select: function(id, label, values, kwargs) {
        var options;
        if (typeof values === 'function') {
            options = values();
        } else {
            options = values.map(function(value) {
                return <option key={value} value={value}>{value}</option>;
            });
        }
        var select = <select className="form-control" id={id} ref={id} onChange={this.handleChange} defaultValue={kwargs.defaultValue}>{options}</select>;
        return this.formField(id, label, select, kwargs);
    },
    radioInlines: function(id, label, values, kwargs) {
        var self = this;
        var defaultValue = kwargs.defaultValue;
        var radios = values.map(function(value) {
          return <label className="radio-inline" key={value}>
                    <input type="radio" ref={id + value} onChange={self.handleChange} key={value} name={label} value={value} defaultChecked={value === defaultValue}/>
                        {value}
                 </label>;
        });
        return this.formField(id, label, radios, kwargs);
    },
    checkbox: function(id, label, values, kwargs) {
        var self = this;
        var defaultValue = kwargs.defaultValue;
        var checkboxes = values.map(function(value) {
          return <label className="checkbox" key={value}>
                    <input type="checkbox" ref={id + value} onChange={self.handleChange} key={value} name={label} value={value} defaultChecked={value === defaultValue}/>
                        {value}
                 </label>;
        });
        return this.formField(id, label, checkboxes, kwargs);
    },
    formField: function(id, label, field) {
        return (
                <div>
                    <div className={'form-group'}>
                        <label htmlFor={id} className="col-sm-4 control-label">{label}</label>
                            <div className={'col-sm-6'}>
                                {field}
                                {this.getValidationMessages(map[id]).map(this.renderHelpText)}
                            </div>
                    </div>
                </div>
            );
    }
});

module.exports = Question;
