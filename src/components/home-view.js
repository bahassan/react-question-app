'use strict';

var React = require('react/addons');
var Question = require('./question');
var $ = require('jQuery');

var HomeView = React.createClass({
    showQuestion: function(fieldId, value) {
        var state = $.grep(this.props.data, function(e){ return e.id === fieldId; })[0];
        state.isHidden = value;
        this.setState(state);
    },
    postQuestion: function(id, answer) {
        var postData = {
            id: id,
            answer: answer
        }, self = this;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/',
            data: postData,
            success: function(){
                self.showQuestion(2, false);
            },
            error: function(){
                self.showQuestion(2, false);
            }
        });
    },
    render: function() {
        var fields = this.props.data.map(function(questionData) {
            var props = {
                id: questionData.id,
                data: questionData,
                onChange: this.postQuestion
            };
            return (questionData.isHidden ? null : <Question key={props.id} {...props} /> );
        }, this);

        return (
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-12'>
                            {fields}
                        </div>
                    </div>
                </div>
        );
    }
});

module.exports = HomeView;
